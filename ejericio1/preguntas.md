# EJERCICIO 1

## 1. Crea un directorio único para ti con mkdir y cámbiate a ese directorio con cd.
	
	mkdir ejercicio1
	cd practica

## 2. ¿Cuál es el costo por hora de una instancia de AWS tipo t2.micro? ¿m5.2xlarge?

	Costo AWS t2..micro $0.0116 por hora, $8.47 por mes

	Costo AWS m5.2xlarge $0.484 por hora (EC2)

## 3. Explica que es una llave pública y una llave privada de ssh.

	Una llave SSH es una credencial de acceso en el protocolo SSH. Su función es similar al nombre de usuario y contraseña, pero las llaves son utilizadas para procesos automatizados y para implementar single sign-on por administradores de sistema y power users.

	La esencia de una llave pública de autentificación es que permite el acceso de un servidor a otro sin tener que escribir un password. Esta característica es poderosa porque es ampliamente usada para transferir archivos (usando el protocolo SFTP) y para administración de configuraciones. También es utilizada por administradores de sistema para hacer un single sign-on.

	Las public keys son llaves que autorizan conceder acceso. Son análogas para a candados que solamente la llave privada correspondiente puede abrir. (Llaves que definen quién puede accesar a un sistema.)

	Las llaves privadas son llaves de identidad que un cliente de SSH puede utilizar para autentificarse al momento de logging en un servidor SSH. Son análogas a las llaves físicas que pueden abrir uno o más candados.

	En conjunto las llaves privadas y públicas se llaman user keys. 


## 4. Analiza el output de los comandos.

	uname -a:

		Linux ip-172-30-0-235 4.4.0-1072-aws #82-Ubuntu SMP Fri Nov 2 15:00:21 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux

		Identifica la máquina virtual, indicando su dirección IP y tipo de máquina. Te proporciona también el Kernel del sistema operativo.

	cat /etc/issue:
		
		Ubuntu 16.04.5 LTS \n \l

		Nos permite conocer la distribución que posee un sistema GNU/Linux.

	¿En la instancia qué distribución y kernel estás trabajando?

		Estamos trabajando en una máquina Linux con una distribución Ubuntu 16.04.5 

## 7. Encuentra todos los archivos que contengan la cadena o patrón "gcc" abajo de /usr/bin

	c89-gcc
	c99-gcc
	gcc
	gcc-5
	gcc-ar
	gcc-ar-5
	gcc-nm
	gcc-nm-5
	gcc-ranlib
	gcc-ranlib-5     

## 8. Explica el significado del comando ls -S ¿Y ls -rS?

	ls : Enlista el contenido de un directorio

	ls -S: Enlista el contenido de un directorio por orden de tamaño. Primero el más grande.

	ls -s: Imprime el tamaño asignado a cada archivo en bloques.

## 9. Explica que es gcc

	gcc - GNU project C and C++ compiler

	Es un compliador que hace el procesamiento, la compilación, el ensamblaje y el linking.

## 10. ¿Cuál es la IP address interna de la instancia de AWS a donde te conectaste? Hint: ifconfig

	Se llama inet addr:172.30.0.235

	eth0      Link encap:Ethernet  HWaddr 02:8a:b5:03:30:80  
	          inet addr:172.30.0.235  Bcast:172.30.0.255  Mask:255.255.255.0
	          inet6 addr: fe80::8a:b5ff:fe03:3080/64 Scope:Link
	          UP BROADCAST RUNNING MULTICAST  MTU:9001  Metric:1
	          RX packets:23309 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:16435 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:1000 
	          RX bytes:7345712 (7.3 MB)  TX bytes:3366938 (3.3 MB)

	lo        Link encap:Local Loopback  
	          inet addr:127.0.0.1  Mask:255.0.0.0
	          inet6 addr: ::1/128 Scope:Host
	          UP LOOPBACK RUNNING  MTU:65536  Metric:1
	          RX packets:200 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:200 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:1 
	          RX bytes:15040 (15.0 KB)  TX bytes:15040 (15.0 KB)

11. Explica que hace df -h

	df : report file system disk space usage

	df -h: imprime la información del disco de manera entendible en potencias de 1024.

12. Explica qué hacen los siguientes comandos dentro de la instancia:

	cat /proc/cpuinfo

	Proporciona información de la máquina.

		processor	: 0
		vendor_id	: GenuineIntel
		cpu family	: 6
		model		: 63
		model name	: Intel(R) Xeon(R) CPU E5-2676 v3 @ 2.40GHz
		stepping	: 2
		microcode	: 0x3d
		cpu MHz		: 2400.060
		cache size	: 30720 KB
		physical id	: 0
		siblings	: 1
		core id		: 0
		cpu cores	: 1
		apicid		: 0
		initial apicid	: 0
		fpu		: yes
		fpu_exception	: yes
		cpuid level	: 13
		wp		: yes
		flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp lm constant_tsc rep_good nopl xtopology pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single kaiser fsgsbase bmi1 avx2 smep bmi2 erms invpcid xsaveopt
		bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf
		bogomips	: 4800.12
		clflush size	: 64
		cache_alignment	: 64
		address sizes	: 46 bits physical, 48 bits virtual
		power management:

	cat /proc/meminfo

	Imprime información sobre la memoria de la máquina

		MemTotal:        1014492 kB
		MemFree:          138816 kB
		MemAvailable:     707236 kB
		Buffers:           69976 kB
		Cached:           588152 kB
		SwapCached:            0 kB
		Active:           438216 kB
		Inactive:         284196 kB
		Active(anon):      67568 kB
		Inactive(anon):     2720 kB
		Active(file):     370648 kB
		Inactive(file):   281476 kB
		Unevictable:        3652 kB
		Mlocked:            3652 kB
		SwapTotal:             0 kB
		SwapFree:              0 kB
		Dirty:                80 kB
		Writeback:             0 kB
		AnonPages:         67968 kB
		Mapped:            52744 kB
		Shmem:              3584 kB
		Slab:             114972 kB
		SReclaimable:      88036 kB
		SUnreclaim:        26936 kB
		KernelStack:        2896 kB
		PageTables:         7012 kB
		NFS_Unstable:          0 kB
		Bounce:                0 kB
		WritebackTmp:          0 kB
		CommitLimit:      507244 kB
		Committed_AS:     362984 kB
		VmallocTotal:   34359738367 kB
		VmallocUsed:           0 kB
		VmallocChunk:          0 kB
		HardwareCorrupted:     0 kB
		AnonHugePages:      4096 kB
		CmaTotal:              0 kB
		CmaFree:               0 kB
		HugePages_Total:       0
		HugePages_Free:        0
		HugePages_Rsvd:        0
		HugePages_Surp:        0
		Hugepagesize:       2048 kB
		DirectMap4k:       43008 kB
		DirectMap2M:     1005568 kB

13. Utiliza el comando wget para transferir el archivo mis_datos.tar.gz a tu directorio de trabajo desde: http://www.matem.unam.mx/~benjamin/mis_datos.tar.gz

	Hay que abrir una carpeta en home con mi nombre y ahí descargo el archivo.

	a. Analiza el contenido sin extraerlo y redirecciona el output de tu comando a un archivo de nombre archivos.txt (hint: argumentos de tar)

		tar -tvf mis_datos.tar.gz

		drwxrwxr-x benjamin/benjamin 0 2018-09-02 04:15 mis_datos/
		drwxrwxr-x benjamin/benjamin 0 2018-09-02 04:14 mis_datos/raw/
		-rw-rw-r-- benjamin/benjamin 61194 2018-09-02 04:13 mis_datos/raw/train.csv
		-rw-rw-r-- benjamin/benjamin  1442 2018-09-02 04:14 mis_datos/raw/crime_data.csv
		drwxrwxr-x benjamin/benjamin     0 2018-09-02 04:15 mis_datos/smae/
		-rw-rw-r-- benjamin/benjamin    62 2018-09-02 03:49 mis_datos/smae/group.txt
		-rw-rw-r-- benjamin/benjamin    65 2018-09-01 21:31 mis_datos/smae/geeks.txt
		-rw-rw-r-- benjamin/benjamin 45289 2014-03-14 14:55 mis_datos/smae/calories.csv
		-rw-rw-r-- benjamin/benjamin 27350 2018-09-02 02:32 mis_datos/smae/Demographic_Statistics_By_Zip_Code.csv

	b. Desempaca solo en directorio mis_datos/smae en tu directorio de trabajo. (mismo hint)

		tar xvf mis_datos.tar.gz

			mis_datos/
			mis_datos/raw/
			mis_datos/raw/train.csv
			mis_datos/raw/crime_data.csv
			mis_datos/smae/
			mis_datos/smae/group.txt
			mis_datos/smae/geeks.txt
			mis_datos/smae/calories.csv
			mis_datos/smae/Demographic_Statistics_By_Zip_Code.csv

## 14. Utiliza el comando find y encuentra todos los archivos en /usr/lib mayores a 5 megabytes de tamaño (Hint: find y argumento size)

	find /usr/lib -size +5M

## 15. Explica que hace el comando

	find /usr/lib/ -name "*gcc*" -exec ls -lah {} \;

	Busca en el /usr/lib/ todos los archivos que contienen gcc. Ejecutables la siguiente función. Imprimir los archivos en formato largo, incluyendo los archivos invisibles y con formatos de Bytes, Kilobytes, Megabytes, Gibabytes para reducir el espacio.

	total 12K
	drwxr-xr-x  3 root root 4.0K Apr 15  2016 .
	drwxr-xr-x 57 root root 4.0K Jan 13 18:46 ..
	drwxr-xr-x  4 root root 4.0K Nov 14 21:46 x86_64-linux-gnu
	-rw-r--r-- 1 root root 6.9K Aug 27 16:51 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint-gcc.h
	-rw-r--r-- 1 root root 51K Aug 27 21:22 /usr/lib/gcc/x86_64-linux-gnu/5/libgcc_eh.a
	lrwxrwxrwx 1 root root 35 Aug 27 21:22 /usr/lib/gcc/x86_64-linux-gnu/5/libgcc_s.so -> /lib/x86_64-linux-gnu/libgcc_s.so.1
	-rw-r--r-- 1 root root 2.9M Aug 27 21:22 /usr/lib/gcc/x86_64-linux-gnu/5/libgcc.a

16. Explica que hace el comando mkdir -p

	mkdir -p, --parents

		no error if existing, make parent directories as needed

17. Guarda tu historial de comandos en un archivo.

	history > comandos.txt
	cat comandos.txt








